use iced::{ executor,
            Application,
            Theme,
            Command,
            Element };
use iced::widget::{ button,
                    column,
                    text,
                    Column,
                    text::Text,
                    button::Button,
                    container::Container };
use iced_lazy::component::Component;
use crate::components::counter::Counter;

pub struct App {
    // state of our application/component
    count: isize,
}

// Essentially, we are building a composable view.
// Essentially, when we push our buttons to the column,
// we are cloning (or copying if type can be stored on a stack)
// our messages to a new element we produce. Hence, we need to
// derive Clone and Copy to do that:
#[derive(Debug, Clone, Copy)]
pub enum Message {
    Increment,
    Decrement,
}

impl Application for App {
    type Executor = executor::Default;
    type Flags = ();
    // Messages: user interactions or meaningful events that you care about
    type Message = Message; 
    type Theme = Theme;

    fn new(_flags: ()) -> (App, Command<Self::Message>) {
        (
            App { count: 0 },
            Command::none()
        )
    }

    fn title(&self) -> String {
        "Hello world!".to_string()
    }

    // update: a way to display your state as widgets that may produce messages on
    // user interactions. Will receive messages of the type we defined earlier and
    // mutate the state of our application
    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::Increment => {
                println!("plus");
                self.count += 1;
            },
            Message::Decrement => {
                println!("minus");
                self.count -= 1;
            },
        };
        Command::none()
    }

    // view: a way to display your state as widgets that may produce messages
    // on user interactions
    fn view(&self) -> Element<Self::Message> {
        Container::new(
            Column::new()
                .push(
                    Column::new()
                        .push(Button::new("Increment").on_press(Message::Increment))
                        .push(Text::new(format!("Count: {}", self.count)))
                        .push(Button::new("Decrement").on_press(Message::Decrement))
                ).push(Counter::new().view())
        )
            .center_x()
            .center_y()
            .width(iced::Length::Fill)
            .height(iced::Length::Fill)
            .into()
        // column![
        //     text("Hello world").size(50),
        //     button("+").on_press(Message::Increment),
        //     text(self.count).size(50),
        //     button("-").on_press(Message::Decrement),
        // ].into()
    }
}

use iced::{ Application,
            Settings };
use crate::app::App;

mod components;
mod app;

fn main() {
    App::run(Settings::default()).unwrap();
}


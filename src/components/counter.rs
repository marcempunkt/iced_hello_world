use iced::{ Element };
use iced::widget::{ self,
                    button,
                    text,
                    row };
use iced_lazy::component::Component;

pub struct Counter {
    value: isize,
}

#[derive(Debug, Clone)]
pub enum Event {
    Increment,
    Decrement,
}

impl Counter {
    pub fn new() -> Self {
        Counter { value: 0 }
    }
}

impl<Message, Renderer> Component<Message, Renderer> for Counter
where
    Renderer: iced_native::text::Renderer + 'static,
    Renderer::Theme: widget::button::StyleSheet
    + widget::text_input::StyleSheet
    + widget::text::StyleSheet
{
    type State = ();
    type Event = Event;

    fn update(
        &mut self,
        state: &mut Self::State,
        event: Self::Event,
    ) -> Option<Message> {
        match event {
            Event::Increment => Some(self.value += 1),
            Event::Decrement => Some(self.value -= 1),
        }
    }

    fn view(
        &self,
        _state: &Self::State
    ) -> Element<Self::Event, Renderer> {
        row![
            button("-").on_press(Event::Decrement),
            text(self.value),
            button("+").on_press(Event::Increment),
        ].into()
    }
}

// STATELESS Components

// #[derive(Clone)]
// pub struct Counter {
//     value: isize,
// }

// #[derive(Debug, Clone, Copy)]
// pub enum Message {
//     Increment,
//     Decrement,
// }

// impl Counter {
//     // Instantiates our view
//     pub fn new() -> Self {
//         Counter{ value: 0 }
//     }

//     // builds our views and return a single Element that would
//     // produce messages of type Message
//     pub fn view(&self) -> Element<Message> {
//         Container::new(
//             Column::new()
//                 .push(Button::new("Counter Component Increment"))
//                 .push(Text::new(format!("Counter component: {}", self.value)))
//                 .push(Button::new("Counter Component Decrement"))
            
//         ).into()
//     }
// }
